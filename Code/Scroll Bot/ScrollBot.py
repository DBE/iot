'''
Created on 27.06.2022

@author: Christian
'''

"""
Code fuer den Scrollbot

"""

import random
import time
from threading import Thread

import scrollphathd
from PIL import Image
scrollphathd.rotate(degrees=180)

import os
from paho.mqtt import client as mqtt_client

dir_path=os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path)

broker = 'broker.emqx.io'
port = 1883
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'
username = 'iot'
password = '1234'

class botVars():
    def __init__(self):
        self.Scollbelegt=False
        




"""def scroll_message(msg):
    print(msg)"""


def scroll_message(message):
    scrollphathd.clear()                         # Clear the display and reset scrolling to (0, 0)
    length = scrollphathd.write_string(message)  # Write out your message
    scrollphathd.show()                          # Show the result
    time.sleep(0.5)                              # Initial delay before scrolling

    length -= scrollphathd.width

    # Now for the scrolling loop...
    while length > 0:
        scrollphathd.scroll(1)                   # Scroll the buffer one place to the left
        scrollphathd.show()                      # Show the result
        length -= 1
        time.sleep(0.02)                         # Delay for each scrolling step

    time.sleep(0.5)                              # Delay at the end of scrolling





def get_pixel(x, y,img):
    p = img.getpixel((x, y))

    if img.getpalette() is not None:
        r, g, b = img.getpalette()[p:p + 3]
        p = max(r, g, b)

    return p / 255.0


def showIMG(path):
    img = Image.open(path)
    scrollphathd.clear()
    scrollphathd.show()
    scrollphathd.rotate(degrees=0)
    try:
        for x in range(0, scrollphathd.DISPLAY_WIDTH):
            for y in range(0, scrollphathd.DISPLAY_HEIGHT):
                brightness = get_pixel(x, y,img)
                scrollphathd.pixel(x, 6 - y, brightness * 1)
        scrollphathd.show()
        time.sleep(0.03)
        #scrollphathd.scroll(-1)
    
    except KeyboardInterrupt:
        scrollphathd.clear()
        scrollphathd.show()
    scrollphathd.rotate(degrees=180)


def subscribeDisp(client: mqtt_client,topic,infos):
    
    def on_message(client, userdata, msg):
        #inmsg[count]=msg.payload.decode()
        nachricht= msg.payload.decode()
        if(nachricht!="online" and nachricht!="frei" and nachricht!="belegt"): #
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
            if(nachricht=="blockIoTScroll"):
                
                infos.Scollbelegt=True
              
                con="connected"
                thread = Thread(target=scroll_message, args=(con,))
                thread.start()
            elif(nachricht=="freeIoTScroll"):
                discon="disconnected"
                thread = Thread(target=scroll_message, args=(discon,))
                thread.start()
                infos.Scollbelegt=False
            elif(nachricht=="smile"):
                showIMG("smile.bmp")
            elif(nachricht=="sad"):
                showIMG("sad.bmp")
            elif(nachricht=="love"):
                showIMG("love.bmp")
            elif(nachricht=="frage"):
                showIMG("fragezeichen.bmp")
            else:
                print(type(msg.payload.decode()))
                print(msg.payload.decode())
                thread = Thread(target=scroll_message, args=(msg.payload.decode(),))
                thread.start()
            

    client.subscribe(topic)
    client.on_message = on_message
   


def subscribe(client: mqtt_client,topic,inmsg,count):
    def on_message(client, userdata, msg):
        if(msg.payload.decode()=="online"):
            inmsg[count]="online"
        #print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")

    client.subscribe(topic)
    client.on_message = on_message
   


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            #print("Connected to MQTT Broker!")
            pass
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def searchMqttHoocks():
    """
    Diese Funktion sucht alle MQTT Hoocks die gerade online senden
    und gibt eine liste mit den bereits verwendeten Hooks zurueck
    
    """
    print("start search")
    test_count=10
    infoList=[]
    start_time=time.time()
    end_time=start_time
    topicstr="IoT/ScrollBot"
    for i in range(test_count):
        infoList.append("offline")
        localTop=topicstr + str(i)
        #print("versuche : " + localTop)
        client = connect_mqtt()
        subscribe(client,localTop,infoList,i)
        client.loop_start()
        start_time=time.time()
        
        while end_time-start_time<1:
            end_time=time.time()
        client.loop_stop()
    print("start done")
    return infoList





def getmyHook():
    """
    generiert den eigenen Hoock namen um einen neuen Channel zu erstellen
    """
    hooklist=searchMqttHoocks()
    print(hooklist)
    myID=0
    for i in range(len(hooklist)):
        print(hooklist[i])
        if hooklist[i]=="offline":
            myID=i
            break
    return "IoT/ScrollBot"+str(myID)


def publish(client,topic,infos):
    print("im online")
    while True:
        
        time.sleep(0.5)
        msg = f"online"
        result = client.publish(topic, msg)
 
        status = result[0]
        if status == 0:
            pass
        else:
            print(f"Failed to send message to topic {topic}")
        
        print(infos.Scollbelegt)
        if infos.Scollbelegt:
            msg = f"belegt"
        else:
            msg = f"frei"
        result = client.publish(topic, msg)
 
        status = result[0]
        if status == 0:
            pass
        else:
            print(f"Failed to send message to topic {topic}")
            
        



def build_my_MQTT_Listener(client, topic,infos):
    print("hoere zu")
    subscribeDisp(client,topic,infos)
    client.loop_forever()
   

if __name__ == '__main__':
    infos =botVars()
    iyTopic = getmyHook()
    print("my topic: " + iyTopic)
    client = connect_mqtt()
    scroll_message("online")
    thread = Thread(target=build_my_MQTT_Listener, args=(client, iyTopic,infos))
    thread.start()
    publish(client,iyTopic,infos)

 
 
 
 
 
 
 
 
