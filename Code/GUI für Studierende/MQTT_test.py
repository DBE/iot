'''
Created on 27.06.2022

@author: Christian
'''

# python 3.6

import os
os.chdir(os.path.dirname(__file__))
import random
import time

from paho.mqtt import client as mqtt_client
from PyQt5 import QtWidgets, uic, QtGui, QtWidgets
import sys

"""
code fuer den bentzer


"""


test_count=2

broker = 'broker.emqx.io'
port = 1883
topic = "IoT/ScrollBot1"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'
username = 'iot'
password = '1234'




def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client,chanel,msg):
    print(msg)
    result = client.publish(chanel, msg)
    # result: [0, 1]
    status = result[0]
    if status == 0:
        print(f"Send `{msg}` to topic `{topic}`")
    else:
        print(f"Failed to send message to topic {topic}")


def subscribe(client: mqtt_client,topic,inmsg,count):
    def on_message(client, userdata, msg):
        if (msg.payload.decode()=="online"):
            inmsg[count][0]="online"
        if (msg.payload.decode()=="frei"):
            inmsg[count][1]="frei"
        #print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")

    client.subscribe(topic)
    client.on_message = on_message


def searchMqttHoocks():
    """
    Diese Funktion sucht alle MQTT Hoocks die gerade online senden
    und gibt eine liste mit den bereits verwendeten Hooks zurueck
    
    """
    print("start search")
    
    infoList=[]
    start_time=time.time()
    end_time=start_time
    topicstr="IoT/ScrollBot"
    for i in range(test_count):
        infoList.append(["offline","belegt"])
        localTop=topicstr + str(i)
        #print("versuche : " + localTop)
        client = connect_mqtt()
        subscribe(client,localTop,infoList,i)
        client.loop_start()
        start_time=time.time()
        
        while end_time-start_time<1:
            end_time=time.time()
        client.loop_stop()
    print("start done")
    return infoList




class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('GUI.ui', self)
        self.myScollBot=""
        
        
        self.pushButton_2.setIcon(QtGui.QIcon("Smileys//lachen.PNG"))
        self.pushButton_3.setIcon(QtGui.QIcon("Smileys//traurig.PNG"))
        self.pushButton_4.setIcon(QtGui.QIcon("Smileys//liebe.PNG"))
        self.pushButton_5.setIcon(QtGui.QIcon("Smileys//Fragezeichen.PNG"))
        
        self.pushButton_2.clicked.connect(self.smile)
        self.pushButton_3.clicked.connect(self.sad)
        self.pushButton_4.clicked.connect(self.love)
        self.pushButton_5.clicked.connect(self.frage)
        
        self.show()
        
        self.pushButton.clicked.connect(self.send_msg)
    
    def smile(self):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,"smile")
        self.client.loop_stop()
    
    def sad(self):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,"sad")
        self.client.loop_stop()
    
    def love(self):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,"love")
        self.client.loop_stop()
    
    def frage(self):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,"frage")
        self.client.loop_stop()
    
    def send_msg(self):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,self.lineEdit.text())
        
        
        self.client.loop_stop()
        
    def blockScrollBot(self):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,"blockIoTScroll")
        self.client.loop_stop()
        
    def closeEvent(self, event):
        self.client = connect_mqtt()
        self.client.loop_start()
        publish(self.client,self.myScollBot,"freeIoTScroll")
        self.client.loop_stop()
        
    
if __name__ == '__main__':
    hooklist=searchMqttHoocks()
    print(hooklist)
    
    myScollBotID=9999;
    for i in range(len(hooklist)):
        if(hooklist[i]==["online","frei"]):
            myScollBotID=i;
            break;
        else:
            print("Es sind gerade alle ScollBots belegt")
    
    if(myScollBotID!=9999):
        myScollBotID="IoT/ScrollBot"+str(myScollBotID)
        print(myScollBotID)
        
        
        app = QtWidgets.QApplication(sys.argv)
        window = Ui()
        window.myScollBot=myScollBotID
        window.blockScrollBot()
        app.exec_()
    else:
        print("leider sind gerade alle ScrollBots belegt")
