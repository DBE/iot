**Inhaltsverzeichnis**

- [00 Einleitung](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/00-Einleitung)<br>
Grober Überblick über die Aufgabenstellung, das Projekt sowie die vorangegangenen Überlegungen

- [01 Team](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/01-Team)<br>
Vorstellung des Teams, deren bisherige Qualifikation sowie die Aufgaben im Projekt

- [02 Konzeption](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/02-Konzeption)<br>
Ist-Zustand, Problematiken, Entwicklungsmethoden, Persona, Value Proposition Canvas

- [03 Prototyp (MVP)](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/03-Materialliste)<br>
Input, Output, Technologien und Services, Netzwerkskizze, Kommunikation

- [04 Bezug zur Vorlesung](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/04-Minimum-Viable-Product-(MVP))<br>
Einbettung, Reactive Room, Design-Prinzipien, Medienbrüche, Produkt-Service Logik

- [05 Inbetriebnahme](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/05-Inbetriebnahme)<br>
Inhalt des Kits, zusätzliche Hardware, Zusammenbau, Inbetriebnahme

- [06 Materialliste](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/06-Bezug-zur-Vorlesung)<br>
Überblick über die Kosten der verwendeten Hardware

- [07 Präsentation](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/07-Pr%C3%A4sentation)<br>
Präsentation zum Abschluss des Hackathons

- [08 Fazit & Ausblick](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/08-Fazit-&-Ausblick)<br>
Fazit des Projekts, Ausblick auf Erweiterungsmöglichkeiten/ Verbesserungen

- [09 Links](https://gitlab.reutlingen-university.de/DBE/iot/-/wikis/09-Links)<br>
Genutzte Medien
